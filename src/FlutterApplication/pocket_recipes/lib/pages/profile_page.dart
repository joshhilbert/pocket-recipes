import 'package:flutter/material.dart';
import 'package:pocket_recipes/pages/user_profile_page.dart';

class ProfilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text("Your Account"),
          backgroundColor: Colors.green,
        ),
        body: UserProfilePage(),
      );
}
